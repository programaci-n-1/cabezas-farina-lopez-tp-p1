package juego;
import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class Ninja {
	
	private double x;
	private double y;
	private double tamaño;
	private double velocidad;
	private Image ninjaPng;
		
	public Ninja(double x, double y, double tamaño, double velocidad) {
		this.x = x;
		this.y = y;
		this.tamaño = tamaño;
		this.velocidad = velocidad;
		this.ninjaPng = Herramientas.cargarImagen("ninja.png");
	}
		
	public void dibujar(Entorno e) {
		e.dibujarImagen(ninjaPng, x, y, 0, 0.09);
	}
		
	public void moverIzquierda() {
		x -= velocidad;
	}

	public void moverDerecha() {
		x += velocidad;
	}
		
	public void moverArriba() {
		y -= velocidad;
	}
		
	public void moverAbajo() {
		y += velocidad;
	}
		
	public boolean chocasteConElEntorno(Entorno e) {
		return x < tamaño / 2 || x > e.ancho() - tamaño / 2 ||
				y < tamaño / 2 || y > e.alto() - tamaño/2;
	}

	public void andaAlOtroLado(Entorno e) {
		if(x > e.ancho() - tamaño / 2 ) {
			x = 0 + tamaño/2;
		}
		if(x < tamaño/2) {
			x = e.ancho() - tamaño;
		}
		if(y > e.alto() - tamaño / 2) {
			y = 0 + tamaño/2;
		}
		if(y < tamaño/2) {
			y = e.alto() - tamaño / 2;
		}	
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}
		
	public void stop() {
		velocidad = 0;	
	}

}
