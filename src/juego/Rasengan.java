package juego;
import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class Rasengan {
	
	private double x;
	private double y;
	private double tamaño;
	private double velocidad;
	private int direccion;
	private Image rasenganPng;
	
	public Rasengan(double x, double y, double tamaño, double velocidad, int direccion) {
		this.x = x;
		this.y = y;
		this.tamaño = tamaño;
		this.velocidad = velocidad;
		this.direccion = direccion;
		this.rasenganPng = Herramientas.cargarImagen("rasengan.png");
	}

	public void dibujar(Entorno e) {
		e.dibujarImagen(rasenganPng, x, y, 0, 0.04);
	}
	
	public boolean chocoConElEntorno(Entorno e) {
		return x < tamaño / 2 || x > e.ancho() - tamaño / 2 ||
				y < tamaño / 2 || y > e.alto() - tamaño / 2;
	}
	
	public void moverIzquierda() {
		x -= velocidad;
	}

	public void moverDerecha() {
		x += velocidad;
	}
	
	public void moverArriba() {
		y -= velocidad;
	}
	
	public void moverAbajo() {
		y += velocidad;
	}

	public void mover() {
		if(this.direccion == 1) {
			this.moverDerecha();
		}
		if(this.direccion == 2) {
			this.moverIzquierda();
		}
		if(this.direccion == 3) {
			this.moverArriba();
		}
		if(this.direccion == 4) {
			this.moverAbajo();
		}
	}
	
	public boolean chocoConUnNinja(Ninja[] n) {
		for (int i=0; i<n.length;i++) {
			if (n[i] != null) {
				if ( x + tamaño > n[i].getX() && x - tamaño < n[i].getX() && 
						y + tamaño > n[i].getY() && y - tamaño < n[i].getY()) {
					return true;
					 // sakuraBordeDer > ninjaBordeIzq, sakuraBordeIzq < ninjaBordeDer
					// sakuraBordeInf > ninjaBordeSup, sakuraBordeSup < ninjaBordeInf
				}
			}
		}
		return false;
	}
	
	public int dameElIDelNinjaChocado(Ninja[] n) {
		for (int i=0; i<n.length;i++) {
			if (n[i] != null) {
				if ( x + tamaño > n[i].getX() && x - tamaño < n[i].getX() && 
						y + tamaño > n[i].getY() && y - tamaño < n[i].getY()) {
					return i;
					 // sakuraBordeDer > ninjaBordeIzq, sakuraBordeIzq < ninjaBordeDer
					// sakuraBordeInf > ninjaBordeSup, sakuraBordeSup < ninjaBordeInf
				}
			}
		}
		return 0;
	}
	
	

}


