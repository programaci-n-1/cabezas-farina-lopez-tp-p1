package juego;

import java.awt.Color;

import entorno.Entorno;

public class Casa {
	
	private int x;
	private int y;
	private int ancho;
	private int alto;
	private double angulo; // No se usa pero la necesita dibujar()
	private Color color;
	
	public Casa(int x, int y, int ancho, int alto, double angulo, Color color) {
		this.x = x;
		this.y = y;
		this.ancho = ancho;
		this.alto = alto;
		this.angulo = angulo;
		this.color = color;
	}
	
	public void dibujar(Entorno e) {
		e.dibujarRectangulo(x, y, ancho, alto, angulo, color);
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getAncho() {
		return ancho;
	}

	public int getAlto() {
		return alto;
	}
	
	

}
