package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Sakura {

	private double x;
	private double y;
	private double tamaño;
	private double velocidad;
	private Image sakuraPng;
	
	public Sakura(double x, double y, double tamaño, Color color, double velocidad) {
		this.x = x;
		this.y = y;
		this.tamaño = tamaño;
		this.velocidad = velocidad;
		this.sakuraPng = Herramientas.cargarImagen("sakura.png");
	}
	
	public void dibujar(Entorno e) {
		e.dibujarImagen(sakuraPng, x, y, 0, 0.15);
	}

	public void moverIzquierda() {
		x -= velocidad;
	}

	public void moverDerecha() {
		x += velocidad;
	}
	
	public void moverArriba() {
		y -= velocidad;
	}
	
	public void moverAbajo() {
		y += velocidad;
	}
	
	public boolean chocasteConElEntorno(Entorno e) {
		return x < tamaño / 2 || x > e.ancho() - tamaño / 2 ||
				y < tamaño / 2 || y > e.alto() - tamaño/2;
	}
	
	public static boolean arribaYabajoTenesManzanas(Manzana[][] m, double x, double y, double tamaño) {
		for (int i = 0; i < m.length; i++) {
			if (x + tamaño / 2 > m[0][i].getX() - m[0][i].getAncho() / 2 && // x + tamaño / 2 = sakuraBordeDer
				x - tamaño / 2 < m[0][i].getX() + m[0][i].getAncho() / 2) { // x - tamaño / 2 = sakuraBordeIzq
				return true; // m[0][i].getX() - m[0][i].getAncho() / 2 = manzanaBordeIzq
							 // m[0][i].getX() + m[0][i].getAncho() / 2 = manzanaBordeDer
			}		
		}
		return false;
	}
	
	public static boolean izqYderTenesManzanas(Manzana[][] m, double x, double y, double tamaño) {
		for (int i = 0; i < m.length; i++) {
			if (y + tamaño / 2 > m[i][0].getY() - m[i][0].getAlto() / 2 && // y + tamaño / 2 = sakuraBordeInf
				y - tamaño / 2 < m[i][0].getY() + m[i][0].getAlto() / 2) { // y - tamaño / 2 = sakuraBordeSup
				return true; // m[i][0].getY() - m[i][0].getAlto() / 2 = manzanaBordeSup
							 // m[i][0].getY() + m[i][0].getAlto() / 2 = manzanaBordeInf
			}
		}
		return false;
	}
	
	public boolean estasChocandoUnaManzanaDesdeAbajo(Manzana[][] m) {
		for (int i = 0; i < m.length; i++) {
				if (arribaYabajoTenesManzanas(m, x, y, tamaño) &&
						y - tamaño / 2 == m[i][0].getY() + m[i][0].getAlto() / 2) { // sakuraBordeSup == manzanaBordeInf
					return true;
				}	
			}
		return false;
	}
	
	public boolean estasChocandoUnaManzanaDesdeArriba(Manzana[][] m) {
		for (int i = 0; i < m.length; i++) {
				if (arribaYabajoTenesManzanas(m, x, y, tamaño) &&
						y + tamaño / 2 == m[i][0].getY() - m[i][0].getAlto() / 2) { // sakuraBordeInf == manzanaBordeSup
					return true;
				}
		}
		return false;
	}
	
	public boolean estasChocandoUnaManzanaDesdeLaIzquierda(Manzana[][] m) {
		for (int i = 0; i < m.length; i++) {
				if (izqYderTenesManzanas(m, x, y, tamaño) &&
						x + tamaño / 2 == m[0][i].getX() - m[0][i].getAncho() / 2) { // sakuraBordeDer == manzanaBordeIzq
					return true;
				}
			}
		return false;
	}
	
	public boolean estasChocandoUnaManzanaDesdeLaDerecha(Manzana[][] m) {
		for (int i = 0; i < m.length; i++) {
				if (izqYderTenesManzanas(m, x, y, tamaño) &&
						x - tamaño / 2 == m[0][i].getX() + m[0][i].getAncho() / 2) { // sakuraBordeIzq == manzanaBordeDer
					return true;
				}
		}
		return false;
	}
	
	
	public void noTeSalgasDelEntorno(Entorno e) {
		if(x > e.ancho() - tamaño / 2 ) {
			x = e.ancho() - tamaño / 2;
		}
		if(x < tamaño/2) {
			x = tamaño/2;
		}
		if(y > e.alto() - tamaño / 2) {
			y = e.alto() - tamaño / 2;
		}
		if(y < tamaño/2) {
			y = tamaño/2;
		}
	}
	
	public boolean chocasteConUnNinja(Ninja[] n) {
		for (int i=0; i<n.length;i++) {
			if (n[i] != null) {
				if ( x + tamaño > n[i].getX() && x - tamaño < n[i].getX() && 
						y + tamaño > n[i].getY() && y - tamaño < n[i].getY()) {
					return true;
					 // sakuraBordeDer > ninjaBordeIzq && sakuraBordeIzq < ninjaBordeDer
					// sakuraBordeInf > ninjaBordeSup && sakuraBordeSup < ninjaBordeInf
				}
			}
		}
		return false;
	}
	
	public boolean tePegóUnShuriken(Shuriken[] s) {
		for (int i=0; i<s.length;i++) {
			if (s[i] != null) {
				if ( x + tamaño > s[i].getX() && x - tamaño < s[i].getX() && 
						y + tamaño > s[i].getY() && y - tamaño < s[i].getY()) {
					return true;
					 // sakuraBordeDer > ninjaBordeIzq && sakuraBordeIzq < ninjaBordeDer
					// sakuraBordeInf > ninjaBordeSup && sakuraBordeSup < ninjaBordeInf
				}
			}
		}
		return false;
	}
	
	public boolean chocasteConUnaMoneda(Moneda[] m) {
		for (int i=0; i<m.length; i++) {
			if (m[i] != null) {
				if (x + tamaño > m[i].getX() && x - tamaño < m[i].getX() && 
						y + tamaño > m[i].getY() && y - tamaño < m[i].getY()) {
					return true;
				}
			}
		}
		return false;
	}
	
	public int dameElIDeLaMonedaTomada(Moneda[] m) {
		for (int i=0; i<m.length;i++) {
			if (m[i] != null) {
				if ( x + tamaño > m[i].getX() && x - tamaño < m[i].getX() && 
						y + tamaño > m[i].getY() && y - tamaño < m[i].getY()) {
					return i;
				}
			}
		}
		return 0;
	}
	
	public boolean llegasteALaCasa(Casa casa) {
		if (casa.getY() < 100) { // si es un casa de las manzanas superiores
			if (y > casa.getY() + casa.getAlto() / 2 &&  // y > casaBordesInf
					y < (casa.getY() + casa.getAlto() / 2) + 40 &&  // y < UnPocoabajoDeCasaBordesInf (por el +40)
					x < casa.getX() + casa.getAncho() / 2 && // x < casaBordeDer
					x > casa.getX() - casa.getAncho() / 2) { // x > casaBordeIzq
						return true;
			}
		} else if (casa.getY() > 500) { // si es una casa de las manzanas inferiores
			if (y < casa.getY() - casa.getAlto() / 2 &&  // y < casaBordesSuperior
					y > (casa.getY() - casa.getAlto() / 2) - 40 &&  // y > unPocoArribaDeCasaBordesSup (por el -40)
					x < casa.getX() + casa.getAncho() / 2 && // x < casaBordeDer
					x > casa.getX() - casa.getAncho() / 2) { // x > casaBordeIzq
						return true;
			}
		} else { // o sino es una casa de las manzanas del medio
			if (y < casa.getY() - casa.getAlto() / 2 &&  // y < casaBordesSuperior
				y > (casa.getY() - casa.getAlto() / 2) - 40 &&  // y > unPocoArribaDeCasaBordesSup (por el -40)
				x < casa.getX() + casa.getAncho() / 2 && // x < casaBordeDer
				x > casa.getX() - casa.getAncho() / 2) { // x > casaBordeIzq
					return true;
			} else if (y > casa.getY() + casa.getAlto() / 2 &&  // y > casaBordesInf
					y < (casa.getY() + casa.getAlto() / 2) + 40 &&  // y < UnPocoabajoDeCasaBordesInf (por el +40)
					x < casa.getX() + casa.getAncho() / 2 && // x < casaBordeDer
					x > casa.getX() - casa.getAncho() / 2) { // x > casaBordeIzq
						return true;
			}
		}
		return false;
	}
	
	public void para() {
		this.velocidad = 0;	
	}

	public double getY() {
		return y;
	}

	public double getX() {
		return x;
	}

	public double getVelocidad() {
		return velocidad;
	}
	
}
